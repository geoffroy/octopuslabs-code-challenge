# Code Challenge for Octopus Labs
-----

## Instructions
create a new directory, cd in it 
and run this command:
```
git clone https://github.com/geoffroygivry/OctopusLabs-Code-Challenge.git
```
then type:
```
docker-compose up
```
when the docker container is built, open your browser and type
```
http://localhost
```
